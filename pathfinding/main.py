import struct
from tkinter import N, NE
import json
from functools import cmp_to_key
from enum import Enum


class Mock:
    path = 'C:/Users/Baptiste/AppData/Local/Ankama/zaap/dofus/content/maps/world-graph.binary'
    playedEntityCellId = 0 #uint
    currentMapId: float = 192416772.0
    linkedZoneRP: int = 17
    
    pathCellsDirections = []

    with open('./data/MapPositions.json') as f:
        mapPositions = json.load(f)

    @classmethod
    def getMapPositionDataById(cls, id: float) -> dict:
        res = [mapPosition for mapPosition in cls.mapPositions if mapPosition['id'] == id]
        if res:
            return res[0]
        else:
            return {}

    @classmethod
    def listMapsByCoordinates(cls, posX: int, posY: int):
        for map in Mock.mapPositions:
            if map["posX"] == posX and map["posY"] == posY:
                print("id : " + str(map["id"]) + ' ' + MapPosition.getMapPositionById(map["id"]).toString())


    @classmethod
    def getPath(cls, mapIdBegin: float, cellIdBegin, mapIdDest: float):
        mapData = WorldPathFinder.dataExtractor.load_dlm_map_by_id(mapIdBegin)
        cell = mapData['cells'][cellIdBegin]
        cellData = CellData(None, cellIdBegin, cell.get("_linkedZone"))

        Mock.currentMapId = mapIdBegin
        Mock.linkedZoneRP = cellData.linkedZoneRP
        WorldPathFinder.findPath(mapIdDest, print)
        return  cls.pathCellsDirections

    print([mapPosition for mapPosition in mapPositions if mapPosition['posX'] == 0][0])


class DirectionsEnum(Enum):
    NORTH = 6
    SOUTH = 2
    EAST = 0
    WEST = 4

class StringUtils:


    @classmethod
    def getAllIndexOf(cls, pStringLookFor: str, pWholeString: str) -> list:
    
        nextIndex: int = 0
        returnedArray: list = []
        usage: int = 0 #uint
        exit: bool = False
        currentIndex: int = 0 #uint

        while not exit:
            nextIndex = pWholeString.find(pStringLookFor,currentIndex) 
            if nextIndex < currentIndex:
                exit = True
            else:
                returnedArray.append(nextIndex)
                currentIndex = nextIndex + len(pStringLookFor)
        return returnedArray

    @classmethod
    def getSingleDelimitedText(cls, pStringEntry: str, pFirstDelimiter: str, pSecondDelimiter: str, pIncludeDelimiter: bool = False) -> str:
        print(pStringEntry)
        firstDelimiterIndex: int = 0
        nextFirstDelimiterIndex: int = 0
        nextSecondDelimiterIndex: int = 0
        numFirstDelimiter: int = 0 #uint
        numSecondDelimiter: int = 0 #uint
        diff: int = 0
        delimitedContent: str = ""
        currentIndex: int = 0 #uint
        secondDelimiterToSkip: int = 0 #uint
        exit: bool = False

        firstDelimiterIndex = pStringEntry.find(pFirstDelimiter, currentIndex)
        if firstDelimiterIndex == -1:
            return ""
        
        currentIndex = firstDelimiterIndex + len(pFirstDelimiter)
        while not exit:
            print("loop")
            nextFirstDelimiterIndex = pStringEntry.find(pFirstDelimiter, currentIndex)
            nextSecondDelimiterIndex = pStringEntry.find(pSecondDelimiter, currentIndex)
            if nextSecondDelimiterIndex == -1:
                exit = True

            if nextFirstDelimiterIndex < nextSecondDelimiterIndex and nextFirstDelimiterIndex != -1:
                secondDelimiterToSkip += len(cls.getAllIndexOf(pFirstDelimiter,pStringEntry[nextFirstDelimiterIndex + len(pFirstDelimiter) : nextSecondDelimiterIndex]))
                currentIndex = nextSecondDelimiterIndex + len(pFirstDelimiter)
                print("now secondDelimiterToSkip", secondDelimiterToSkip)

            elif secondDelimiterToSkip > 1:
                currentIndex = nextSecondDelimiterIndex + len(pSecondDelimiter)
                secondDelimiterToSkip-=1
            else:
                delimitedContent = pStringEntry[firstDelimiterIndex:nextSecondDelimiterIndex + len(pSecondDelimiter)]
                exit = True

        if delimitedContent != "":
            if not pIncludeDelimiter:
                delimitedContent = delimitedContent[len(pFirstDelimiter):]
                delimitedContent = delimitedContent[: len(delimitedContent) - len(pSecondDelimiter)]
            else:
                numFirstDelimiter = len(cls.getAllIndexOf(pFirstDelimiter,delimitedContent))
                numSecondDelimiter = len(cls.getAllIndexOf(pSecondDelimiter,delimitedContent))
                diff = numFirstDelimiter - numSecondDelimiter
                if diff > 0:
                    while diff > 0:
                        firstDelimiterIndex = delimitedContent.find(pFirstDelimiter)
                        nextFirstDelimiterIndex = delimitedContent.find(pFirstDelimiter,firstDelimiterIndex + len(pFirstDelimiter))
                        delimitedContent = delimitedContent[nextFirstDelimiterIndex:]
                        diff-=1

                elif diff < 0:
                    while diff < 0:
                        delimitedContent = delimitedContent[: delimitedContent.rfind(pSecondDelimiter)]
                        diff+=1

        return delimitedContent

    @classmethod
    def getDelimitedText(cls, pText: str, pFirstDelimiter: str, pSecondDelimiter: str, pIncludeDelimiter: bool = False) -> list[str]:
    
        delimitedText: str = None
        firstPart: str = None
        secondPart: str = None
        returnedArray: list[str] = []
        exit: bool = False
        text: str = pText

        while not exit:
        
            delimitedText = cls.getSingleDelimitedText(text, pFirstDelimiter, pSecondDelimiter,pIncludeDelimiter)
            if delimitedText == "":
                exit = True
            else:
                returnedArray.append(delimitedText)
                if not pIncludeDelimiter:
                    delimitedText = pFirstDelimiter + delimitedText + pSecondDelimiter

                firstPart = text[:text.find(delimitedText)]
                while firstPart.find(pFirstDelimiter) != -1:
                    firstPart = firstPart.replace(pFirstDelimiter,"")
                    
                secondPart = text[text.find(delimitedText) + len(delimitedText) :]
                text = firstPart + secondPart
            return returnedArray

class IDataInput:

    def __init__(self, path: str):
        self.f = open(Mock.path, 'rb')

    def readInt(self):
        return struct.unpack('>i', self.f.read(4))[0]

    def readDouble(self):
        return struct.unpack('>d', self.f.read(8))[0]

    def readByte(self):
        return struct.unpack('>b', self.f.read(1))[0]
    
    def readShort(self) -> int:
        return struct.unpack('>i', self.f.read(2))[0]

    def readUnsignedByte(self):
        return struct.unpack('>B', self.f.read(1))[0]

class ItemCriterion:

    def __init__(self, pCriterion: str):
        self._serverCriterionForm = pCriterion

    def get_isRespected(self):
        player:PlayedCharacterManager = PlayedCharacterManager.getInstance()
        if not player or not player.characteristics:
            return True
        return self._operator.compare(self.getCriterion(),self._criterionValue)

    def get_basicText(self):
        return self._serverCriterionForm

    basicText = property(fget=get_basicText)

    isRespected = property(fget=get_isRespected)

class ItemCriterionFactory:

    def __init__(self):
        pass

    @classmethod
    def create(cls, pServerCriterionForm: str) -> ItemCriterion:
        '''
            Oversimplified function
        '''
        return ItemCriterion(pServerCriterionForm)

class IItemCriterion: #interface
    
    def get_inlineCriteria(self) -> list:
        pass
    inlineCriteria = property(fget=get_inlineCriteria)

    def get_isRespected(self) -> bool:
        pass
    isRespected = property(fget=get_isRespected)

    def get_text(self) -> str:
        pass
    text = property(fget=get_text)

    def get_basicText(self) -> str:
        pass
    basicText = property(fget=get_basicText)

    def get_clone(self):
        pass
    clone = property(fget=get_clone)

class GroupItemCriterion:

    def __init__(self, pCriterion: str):
        # super()

        self._malformated = False
        self._singleOperatorType = False

        self._criterionTextForm = pCriterion
        self._cleanCriterionTextForm = self._criterionTextForm
        if not pCriterion:
            return
        self._cleanCriterionTextForm = self._cleanCriterionTextForm.replace(" ","")
        
        delimitedArray: list[str] = StringUtils.getDelimitedText(self._cleanCriterionTextForm,"(",")",True)
        if len(delimitedArray) > 0 and delimitedArray[0] == self._cleanCriterionTextForm:
            self._cleanCriterionTextForm = self._cleanCriterionTextForm[1:]
            self._cleanCriterionTextForm = self._cleanCriterionTextForm[: len(self._cleanCriterionTextForm) - 1]

        self.split()
        self.createNewGroups()

    def split(self):
        criterion:IItemCriterion = None
        indexOfNextCriterion: int = 0
        index: int = 0
        op: str = None
        criterion2: IItemCriterion = None
        indexOfNextCriterion2: int = 0
        index2: int = 0
        firstPart: str = None
        secondPart: str = None
        operator: str = None

        if not self._cleanCriterionTextForm:
            return
        CRITERION: int = 0 #uint
        OPERATOR: int = 1 #uint
        next: int = CRITERION #uint
        exit: bool = False
        searchingString: str = self._cleanCriterionTextForm
        self._criteria: list[IItemCriterion] = []
        self._operators: list[str] = []
        andIndexes: list = StringUtils.getAllIndexOf("&",searchingString)
        orIndexes: list = StringUtils.getAllIndexOf("|",searchingString)

        if len(andIndexes) == 0 or len(orIndexes) == 0:
            self._singleOperatorType = True
            while not exit:
                criterion = self.getFirstCriterion(searchingString)
                if not criterion:
                    indexOfNextCriterion = searchingString.find("&")
                    if indexOfNextCriterion == -1:
                        indexOfNextCriterion = searchingString.find("|")
                    if indexOfNextCriterion == -1:
                        searchingString = ""
                    else:
                        searchingString = searchingString[indexOfNextCriterion + 1:]
                else:
                    self._criteria.append(criterion)
                    index = searchingString.find(criterion.basicText)
                    op = searchingString[index + len(criterion.basicText) : index + 1 + len(criterion.basicText)]
                    if op:
                        self._operators.append(op)
                    searchingString = searchingString[index + 1 + len(criterion.basicText) : ]

                if not searchingString:
                    exit = True
        else:
            while not exit:
                if not searchingString:
                    exit = True
                elif next == CRITERION:
                    criterion2 = self.getFirstCriterion(searchingString)
                    if not criterion2:
                        indexOfNextCriterion2 = searchingString.find("&")
                        if indexOfNextCriterion2 == -1:
                            indexOfNextCriterion2 = searchingString.find("|")
                        if indexOfNextCriterion2 == -1:
                            searchingString = ""
                        else:
                            searchingString = searchingString[indexOfNextCriterion2 + 1 : ]
                    else:
                        self._criteria.append(criterion2)
                        next = OPERATOR
                        index2 = searchingString.find(criterion2.basicText)
                        firstPart = searchingString[:index2]
                        secondPart = searchingString[index2 + len(criterion2.basicText)]
                        searchingString = firstPart + secondPart

                    if not searchingString:
                        exit = True
                else:
                    operator = searchingString[0:1]
                    if not operator:
                        exit = True
                    else:
                        self._operators.append(operator)
                        next = CRITERION
                        searchingString = searchingString[1 : ]

            self._singleOperatorType = self.checkSingleOperatorType(self._operators)
            if len(self._operators) >= len(self._criteria) and (len(self._operators) > 0 and len(self._criteria) > 0):
                self._malformated = True

    def createNewGroups(self):
        crit:IItemCriterion = None
        ope: str = None
        curIndex: int = 0
        exit: bool = False
        crits: list[IItemCriterion] = None
        ops: list[str] = None
        group: GroupItemCriterion = None

        if self._malformated or not self._criteria or len(self._criteria) <= 2 or self._singleOperatorType:
            return
        
        copyCriteria: list[IItemCriterion] = []
        copyOperators: list[str] = []
        
        for crit in self._criteria:
            copyCriteria.append(crit.clone())
        for ope in self._operators:
            copyOperators.append(ope)

        curIndex = 0
        exit = False

        while not exit:
            if len(copyCriteria) <= 2:
                exit = True
            else:
                if copyOperators[curIndex] == "&":
                    crits = []
                    crits.append(copyCriteria[curIndex])
                    crits.append(copyCriteria[curIndex + 1])
                    ops = [copyOperators[curIndex]]
                    group = GroupItemCriterion.create(crits,ops)
                    copyCriteria = copyCriteria[:curIndex] + [group] + copyCriteria[curIndex+2:]
                    copyOperators = copyOperators[:curIndex] + copyOperators[curIndex+1:]
                    curIndex-=1
                curIndex+=1
                if curIndex >= len(copyOperators):
                    exit = True

        self._criteria = copyCriteria
        self._operators = copyOperators
        self._singleOperatorType = self.checkSingleOperatorType(self._operators)

    def checkSingleOperatorType(self, pOperators: list[str]) -> bool:
        op: str = None
        if len(pOperators) > 0:
            for op in pOperators:
               if op != pOperators[0]:
                   return False
        return True
    
    def getFirstCriterion(self, pCriteria: str) -> IItemCriterion:
        criterion: IItemCriterion = None
        dl: list[str] = None
        ANDindex: int = 0
        ORindex: int = 0

        if not pCriteria:
            return None
        
        pCriteria = pCriteria.replace(" ","")
        if pCriteria[0:1] == "(":
            dl = StringUtils.getDelimitedText(pCriteria,"(",")",True)
            criterion = GroupItemCriterion(dl[0])
        else:
            ANDindex = pCriteria.find("&")
            ORindex = pCriteria.find("|")
            if ANDindex == -1 and ORindex == -1:
                criterion = ItemCriterionFactory.create(pCriteria)
            elif (ANDindex < ORindex or ORindex == -1) and ANDindex != -1:
               criterion = ItemCriterionFactory.create(pCriteria.split("&")[0])
            else:
               criterion = ItemCriterionFactory.create(pCriteria.split("|")[0])
        return criterion
            
    def get_isRespected(self):
        print("_criteria", self._criteria)
        if not self._criteria or not len(self._criteria):
            return True
        
        player: PlayedCharacterManager = PlayedCharacterManager.getInstance()
        if not player or not player.characteristics:
            return True
        
        if self._criteria and len(self._criteria) == 1 and isinstance(self._criteria[0], ItemCriterion):
            return self._criteria[0].isRespected
        
        if len(self._operations) and self._operators[0]== "|":
            for criterion in self._criteria:
                if criterion.isRespected:
                    return True
            return False
        
        for criterion in self._criteria:
            if not criterion.isRespected:
                return False
        return True

    isRespected = property(fget=get_isRespected)

    def get_basicText(self) -> str:
        return self._criterionTextForm
    
    basicText = property(fget=get_basicText)

class Vertex: 
    def __init__(self, mapId, zoneId, uid):
        self.mapId, self.zoneId, self.uid = mapId, zoneId, uid
        
    def toString(self):
        return "[Vertex] mapid : " + str(self.mapId) + ", zoneId : " + str(self.zoneId) + ", uid : " + str(self.uid) + ", mapPosition: " + MapPosition.getMapPositionById(self.mapId).toString()

class MapPosition:
    ###
    #   FULL CUSTOM
    ###
    def __init__(self, data: dict):
        self.id: float = data["id"]
        self.posX: int = data["posX"]
        self.posY: int = data["posY"]
        self.outdoor: bool = data["outdoor"]
        self.capabilities: int = data["capabilities"]
        self.nameId: int = data["nameId"]
        # self.showNameOnFingerpost: bool = data["showNameOnFingerpost"]
        self.playlists: list[int] = data["playlists"]
        self.subAreaId: int = data["subAreaId"]
        self.worldMap: int = data["worldMap"]
        self.hasPriorityOnWorldmap: bool = data["hasPriorityOnWorldmap"]
        self.allowPrism: bool = data["allowPrism"]
        self.isTransition: bool = data["isTransition"]
        self.mapHasTemplate: bool = data["mapHasTemplate"]
        self.tacticalModeTemplateId: int = data["tacticalModeTemplateId"] #uint
        self.hasPublicPaddock: bool = data["hasPublicPaddock"]

        self._name = None
        self._subArea = None

    @classmethod
    def getMapPositionById(cls, id: float):
        #CUSTOM
        data = Mock.getMapPositionDataById(id)
        if data:
            return MapPosition(data)
        else:
            return None
        # return GameData.getObject(MODULE,id) as MapPosition

    def toString(self):
        return "posX " + str(self.posX) + " posY " + str(self.posY) + " outdoor " + str(self.outdoor) + " worldMap " + str(self.worldMap) + " subAreaId " + str(self.subAreaId)

class Node:
    def __init__(self, vertex: Vertex, map: MapPosition, cost: int=0, heuristic: int=0, parent=None):
        self.vertex, self.map, self.cost, self.heuristic, self.parent = vertex, map, cost, heuristic, parent

class Edge:
    def __init__(self, origin: Vertex, dest: Vertex):
        self.origin, self.dest = origin, dest
        self.transitions : list[Vertex] = []


    def addTransition(self, dir: int, type: int, skill: int, criterion: str, transitionMapId: int, cell: int, id: int):
        self.transitions.append(Transition(type,dir,skill,criterion,transitionMapId,cell,id))
    
    def toString(self):
        ret = "origin : " + self.origin.toString() + "\ndest : " + self.dest.toString() + '\n'
        for tran in self.transitions:
            ret += tran.toString()
        ret += "\n**********\n"
        return ret

class Transition:
    def __init__(self, type: int, direction: int, skillId: int, criterion: str, transitionMapId: int, cell: int, id: int):
        self.type, self.direction, self.skillId, self.criterion, self.transitionMapId, self.cell, self.id = type, direction, skillId, criterion, transitionMapId, cell, id

    def toString(self):
        return "type: " + str(self.type) + " direction " + str(self.direction) + " skillId " + str(self.skillId) + " criterion " + str(self.criterion) + " transitionMapId " + str(self.transitionMapId) + " cell " + str(self.cell) + " id " + str(self.id)

class WorldGraph:
    def __init__(self, path: str):

        self._vertexUid: int = 0

        self.begin: Vertex = None
        self.dest: Vertex = None
        self.edge: Edge = None
        self.transitionCount: int = 0
        self.j: int = 0
        self._vertices = {}
        self._edges = {}
        self._outgoingEdges = {}

        with open(path, 'rb') as f:
            edgeCount: int = struct.unpack('>i', f.read(4))[0]
            # print("edges_count= "+str(edgeCount))

            for _ in range(edgeCount):
                begin = self.addVertex(struct.unpack('>d', f.read(8))[0], struct.unpack('>i', f.read(4))[0])
                dest = self.addVertex(struct.unpack('>d', f.read(8))[0], struct.unpack('>i', f.read(4))[0])
                edge = self.addEdge(begin, dest)
                transitionCount = struct.unpack('>i', f.read(4))[0]

                for _ in range(transitionCount):
                    dir, type, skill = struct.unpack('>b', f.read(1))[0], struct.unpack('>b', f.read(1))[0], struct.unpack('>i', f.read(4))[0]
                    length = struct.unpack('>i', f.read(4))[0]
                    criterion = f.read(length).decode("utf-8")
                    # if criterion:
                    #     print("criterion", criterion)
                    
                    transitionMapId, cell, id = struct.unpack('>d', f.read(8))[0], struct.unpack('>i', f.read(4))[0], struct.unpack('>d', f.read(8))[0]
                    edge.addTransition(dir, type, skill, criterion, transitionMapId, cell, id)
    
    def addVertex(self, mapId: int, zone: int) -> Vertex:
        if mapId not in self._vertices:
            self._vertices[mapId] = {}
        if zone not in self._vertices[mapId]:
            self._vertices[mapId][zone] = Vertex(mapId, zone, self._vertexUid)
            self._vertexUid += 1
        return self._vertices[mapId][zone]
    
    def getVertex(self, mapId: int, mapRpZone: int) -> Vertex:
        if mapId not in self._vertices:
            return None
        if mapRpZone not in self._vertices[mapId]:
            return None
        return self._vertices[mapId][mapRpZone]

    def getEdge(self, begin: Vertex, dest: Vertex) -> Edge:
        if begin.uid not in self._edges:
            return None
        if dest.uid not in self._edges[begin.uid]:
            return None
        return self._edges[begin.uid][dest.uid]

    def addEdge(self, begin: Vertex, dest: Vertex) -> Edge:
        edge: Edge = self.getEdge(begin, dest)
        if edge:
            return edge
        if((not self.doesVertexExist(begin)) or (not self.doesVertexExist(dest))):
            return None
        edge = Edge(begin, dest)
        if begin.uid not in self._edges:
            self._edges[begin.uid] = {}
        self._edges[begin.uid][dest.uid] = edge
        if begin.uid not in self._outgoingEdges:
            self._outgoingEdges[begin.uid] = []
        outgoing: list[Edge] = self._outgoingEdges[begin.uid]
        outgoing.append(edge)
        return edge

    def getOutgoingEdgesFromVertex(self, begin: Vertex) -> list[Edge]:
        if begin.uid in self._outgoingEdges:
            return self._outgoingEdges[begin.uid]
        return None

    def doesVertexExist(self, v: Vertex) -> bool:
        if v.mapId not in self._vertices:
            return False
        if v.zoneId not in self._vertices[v.mapId]:
            return False
        return True

    def getOutGoingTransitions(self, discard=[], noskill=True, directions: list = [DirectionsEnum], mapIds = []) -> list[Transition]:
        result = []
        v = WorldPathFinder().getCurrentPlayerVertex()
        outgoingEdges = self.worldGraph.getOutgoingEdgesFromVertex(v)
        for e in outgoingEdges:
            for tr in e.transitions:
                if tr.transitionMapId not in discard:
                    if noskill and tr.skillId > 0:
                        continue
                    if directions and (tr.direction < 0 or DirectionsEnum(tr.direction) not in directions):
                        continue
                    if mapIds and tr.transitionMapId not in mapIds:
                        continue
                    result.append(tr)
        return result

    def changeMapToDstDirection(self, direction: DirectionsEnum, discard: list[int] = []) -> None:
        transitions = self.getOutGoingTransitions(discard=discard, directions=[direction])
        if len(transitions) == 0:
            raise Exception(f"No transition found towards direction '{direction.name}'")
        self.sendClickAdjacentMsg(transitions[0].transitionMapId, transitions[0].cell)

    def changeMapToDstCoords(self, x: int, y: int, discard: list[int] = []):
        transitions = self.getOutGoingTransitions(discard)
        if len(transitions) == 0:
            raise Exception(f"No transition found towards coords '{x, y}'")
        for tr in transitions:
            mp = MapPosition.getMapPositionById(tr.transitionMapId)
            if mp.posX == x and mp.posY == y:
                self.sendClickAdjacentMsg(tr.transitionMapId, tr.cell)
                return tr.transitionMapId
        return -1


##########
class IEntity:
    pass

class PlayedCharacterManager(IEntity):
    
    def __init__(self):
        pass

    @classmethod
    def getInstance(cls):
        pass

# playerCell: CellData = MapDisplayManager.getInstance().getDataMapContainer().dataMap.cells[playedEntityCellId]

class Map:
    pass

class CellData:
    # Incomplete version of com.ankamagames.atouin.data.map.CellData, in order to use WorldPathFinder

    def __init__(self, map: Map, cellId: int, linkedZone: int):
        self._map, self.cellId, self._linkedZone = map, cellId, linkedZone
        
        self._losmov: int = 3
        self._floor: int = 0
        self._map: Map = None
        self._arrow: int = 0
        self._mov: bool = False
        self._los : bool = False
        #et tout plein de choses

    def get_linkedZoneRP(self) -> int:
        return (self._linkedZone & 240) >> 4

    def get_map(self) -> Map:
        """
            NOT USED SO FAR, we don't have a Map object
        """
        return self._map


    linkedZoneRP = property(fget=get_linkedZoneRP)
    # linkedZoneFight=property()
    map = property(fget=get_map)

    def fromRaw(self, raw: IDataInput):
        tmpbytesv9: int = 0
        topArrow = False
        bottomArrow = False
        rightArrow =  False
        leftArrow = False
        tmpBits: int = 0
        try:
            self._floor = raw.readByte() * 10
            if self._floor == -1280:
                return
            if self._map.mapVersion >=9:
                tmpbytesv9 = raw.readShort()
                #//
            
            else:
                self._losmov = raw.readUnsignedByte()
                #//

            self.speed = raw.readByte()
            #//
            self.mapChangeData = raw.readByte()
            #//

            if self._map.mapVersion > 5:
                self.moveZone = raw.readUnsignedByte()
                #//
            
            if self._map.mapVersion > 10 and (self.hasLinkedZoneRP() or self.hasLinkedZoneFight()):
                self._linkedZone = raw.readUnsignedByte()
                #//

            if self._map.mapVersion > 7 and self.map.mapVersion < 9:
                tmpBits = raw.readByte()
                #//

        except:
            pass

class MapDisplayManager:
    
    @classmethod
    def getInstance(cls):
        pass
##########


class WorldPathFinder:

    #   protected static const _log:Logger = Log.getLogger(getQualifiedClassName(WorldPathFinder));
    playedCharacterManager:PlayedCharacterManager = None
    worldGraph: WorldGraph = None
    callback = None
    begin: Vertex = None
    dest: int = None
    linkedZone: int = 17

    def __init__(self):
        pass

    @classmethod
    def init(cls, dataExtractor):
        if cls.isInitialized():
            return
        
        cls.dataExtractor = dataExtractor
        cls.playedCharacterManager = PlayedCharacterManager.getInstance()
        cls.setData(Mock.path) # FileLoader.loadExternalFile(XmlConfig.getInstance().getEntry("config.data.pathFinding"),WorldPathFinder.setData);

    @classmethod
    def getWorldGraph(cls) -> WorldGraph:
        return cls.worldGraph

    @classmethod
    def isInitialized(cls) -> bool:
        return cls.worldGraph is not None
    
    @classmethod
    def setData(cls, path: str):
        cls.worldGraph = WorldGraph(path)

    @classmethod
    def findPath(cls, destinationMapId: float, callback):
        
        print(MapPosition.getMapPositionById(Mock.currentMapId).toString())
        print(MapPosition.getMapPositionById(destinationMapId).toString())


        if not cls.isInitialized():
            callback(None)
            return
        
        #  playedCharacterManager = PlayedCharacterManager.getInstance();
        #  _log.info("Start searching path to " + destinationMapId.toString());
        #  TimeDebug.reset();
        #  var playedEntity:IEntity = DofusEntities.getEntity(playedCharacterManager.id);
        #  if(!playedEntity)
        #  {
        #     callback(null);
        #     return;
        #  }
        #  var playedEntityCellId:uint = playedEntity.position.cellId;
        
        # playedEntityCellId = Mock.playedEntityCellId
        # playerCell: CellData = MapDisplayManager.getInstance().getDataMapContainer().dataMap.cells[playedEntityCellId]
        
        cls.begin = cls.worldGraph.getVertex(Mock.currentMapId, Mock.linkedZoneRP)
        print("BEGIN", cls.begin.toString())
        # cls.begin = cls.worldGraph.getVertex(cls.playedCharacterManager.currentMap.mapId, playerCell.linkedZoneRP)
        
        if not cls.begin:
            callback("Couldn't find the beginning")
            return
        
        cls.linkedZone = 1 # Just a counter of map(zone?) changes ?
        cls.callback = callback
        cls.dest = destinationMapId
        cls.next()

    @classmethod
    def abortPathSearch(cls):
        AStar.stopSearch()

    @classmethod
    def onAStarComplete(cls, path: list[Edge]):
        cb = None
        if not path:
            cls.next()
        else:
            cb = cls.callback
            cls.callback = None
            cb(path)

    @classmethod
    def next(cls):
        cb = None
        dest: Vertex = cls.worldGraph.getVertex(cls.dest, cls.linkedZone)
        cls.linkedZone+=1

        if not dest:
            cb = cls.callback
            cls.callback = None
            cb(None)
            return

        AStar.search(cls.worldGraph, cls.begin, dest, cls.onAStarComplete)

class AStar:

    destMap: MapPosition = None #called "dest" in AS3
    closedDic: dict = None
    openList: list[Node] = None
    openDic: dict = None
    iterations: int = None
    worldGraph: WorldGraph = None
    dest: Vertex = None #called "to" in AS3
    callback = None
    _forbiddenSubareaIds: list[int] = None #list[uint]
    HEURISTIC_SCALE: int = 1
    INDOOR_WEIGHT: int = 0
    MAX_ITERATION: int = 10000

    def __init__(self):
        pass

    @classmethod
    def search(cls, worldGraph, begin: Vertex, dest: Vertex, callback):
        print("[AStar] Begin pathfinding")
        if cls.callback:
            print("Pathfinding already in progress")
        if begin==dest:
            callback(None)
            return

        cls.initForbiddenSubareaList()
        cls.worldGraph = worldGraph
        cls.dest = dest
        cls.callback = callback
        cls.destMap = MapPosition.getMapPositionById(dest.mapId)
        cls.closedDic = {}
        cls.openList = []
        cls.openDic = {}
        cls.iterations = 0
        cls.openList.append(Node(begin, MapPosition.getMapPositionById(begin.mapId)))
        cls.compute() # EnterFrameDispatcher.addEventListener(compute,EnterFrameConst.COMPUTE_ASTAR);


    @classmethod
    def initForbiddenSubareaList(cls):
        #  _forbiddenSubareaIds = GameDataQuery.queryEquals(SubArea,"mountAutoTripAllowed",False);
        return

    @classmethod
    def stopSearch(cls):
        pass

    @classmethod
    def compute(cls):
        current: Node = None
        edges: list[Edge] = None
        oldLength: int = 0
        cost: int = 0
        edge: Edge = None
        existing: Node = None
        map: MapPosition = None
        manhattanDistance: int = 0
        node: Node = None
        # start: int = getTimer();
        
        while len(cls.openList) > 0:
            if cls.iterations > cls.MAX_ITERATION:
                cls.callbackWithResult(None)
                print("Too many iterations, aborting A*")
                return
            cls.iterations+=1

            current = cls.openList.pop(0)
            cls.openDic[current.vertex] = None
            if current.vertex == cls.dest:
                print("Goal reached with ", str(cls.iterations)+ " iterations")
                cls.callbackWithResult(cls.buildResultPath(cls.worldGraph,current))
                return
            
            edges = cls.worldGraph.getOutgoingEdgesFromVertex(current.vertex)
            oldLength = len(cls.openList)
            cost = current.cost + 1

            if not edges:
                print("Vertex without outgoing edges ...")
                continue

            for edge in edges:
                if cls.hasValidTransition(edge):
                    if cls.hasValidDestinationSubarea(edge):
                        if edge.dest not in cls.openDic or (cls.openDic[edge.dest] and cost < cls.openDic[edge.dest].cost):
                            map = MapPosition.getMapPositionById(edge.dest.mapId)
                            if not map:
                                print("La map " + str(edge.dest.mapId) + " ne semble pas exister")
                            else:
                                manhattanDistance = abs(map.posX - cls.destMap.posX) + abs(map.posY - cls.destMap.posY)
                                node = Node(edge.dest, map, cost, cost + cls.HEURISTIC_SCALE * manhattanDistance + ( cls.INDOOR_WEIGHT if (current.map.outdoor and not map.outdoor) else 0), current)
                                cls.openList.append(node)
                                cls.openDic[node.vertex] = node
            
            cls.closedDic[current.vertex] = current
            if oldLength < len(cls.openList):
                cls.openList = sorted(cls.openList, key=cmp_to_key(cls.orderNodes))
            # if(getTimer() - start > 1000 / StageShareManager.stage.frameRate)
            # {
            #    return;
            # } 
        
        cls.callbackWithResult(None)


    @classmethod
    def hasValidTransition(cls, edge: Edge) -> bool:
        # print("[AStar] hasValidTransition")
        transition: Transition = None
        criterionWhiteList: list[str] = ["Ad","DM","MI","Mk","Oc","Pc","QF","Qo","Qs","Sv"]
        valid: bool = False
        _loc6_:int = 0
        _loc7_ = edge.transitions

        # print(len(_loc7_), "transition(1) for edge", edge.toString())
        for transition in _loc7_:
            # print("criterion", transition.criterion)
            if transition.criterion and len(transition.criterion) != 0:
                if transition.criterion.find("&") == -1 and transition.criterion.find("|") == -1 and transition.criterion[:2] in criterionWhiteList:
                    return False
                criterion = GroupItemCriterion(transition.criterion)
                return criterion.isRespected

            valid = True
        
        return valid
    
    @classmethod
    def hasValidDestinationSubarea(cls, edge: Edge) -> bool:
        """
            Shoud we consider this function ?
        """
        return True
        fromMapId: id = edge.begin.mapId
        fromSubareaId: int = MapPosition.getMapPositionById(fromMapId).subAreaId
        toMapId: int = edge.dest.mapId
        toSubareaId: int = MapPosition.getMapPositionById(toMapId).subAreaId
        if fromSubareaId == toSubareaId:
            return True
        if cls._forbiddenSubareaIds.find(toSubareaId) != -1:
            return False
        return True

    @classmethod
    def callbackWithResult(cls, result: list[Edge]):
        cb = cls.callback
        cls.callback = None
        # EnterFrameDispatcher.removeEventListener(compute);
        cb(result)

    @classmethod
    def orderNodes(cls, a: Node, b: Node) -> int:
        return 0 if a.heuristic == b.heuristic else (1 if a.heuristic > b.heuristic else -1)

    @classmethod
    def buildResultPath(cls, worldGraph: WorldGraph, node: Node) -> list[Edge]:
        result: list[Edge] = []
        Mock.pathCellsDirections = []
        while node.parent != None:
            result.append(worldGraph.getEdge(node.parent.vertex, node.vertex))
            node = node.parent
        
        for edge in result[::-1]:
            print(edge.toString())
            for transition in edge.transitions:
                Mock.pathCellsDirections.append((transition.cell, transition.direction))

        print(f'\n\nPath in {len(result)} map changes\n\n')
        return result[::-1]



######################
if __name__ == '__main__':
    pass
    # WorldPathFinder.init()

    # Mock.listMapsByCoordinates(-2, 4)
    # Mock.listMapsByCoordinates(-8, -3)
    # Mock.listMapsByCoordinates(9, -16)

    # startMapId = 154010372.0 # Incarnam [-1, -4]
    # startCellId = 75 # Arbitrary choice
    # startLinkedZone = 17
    # print("startLinkedZone", startLinkedZone)

    # startCellData = CellData(startMapId, startCellId, startLinkedZone)

    # endMapId = 154010884.0 # Incarnam [-2, -4]
    # for step in Mock.getPath(startMapId, startCellData, endMapId): #3 -19 outdoor to 5 -19 outdoor
    #     print(step)
    # steps = Mock.getPath(193331717.0, 193331718.0)