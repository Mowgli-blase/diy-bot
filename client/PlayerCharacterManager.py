import logging

class PlayedCharacterManager:
    '''
        Adapted from com.ankamagames.dofus.logic.game.common.managers
    '''

    #private static var _self:PlayedCharacterManager;
    _log = logging.getLogger("PlayedCharacterManager")
      
    def __init__(self, playerName: str):
        self._playerName = playerName
        self._id = None
        # self._currentSubArea = None
        # self._currentWorldMap = None
        # self._currentMap = None
        self._currentCellId = None

    def get_playerName(self) -> str:
        return self._playerName
    
    playerName = property(fget=get_playerName)

    def get_id(self) -> int:
        return self._id

    def set_id(self, id: int):
        if id:
            self._id = id

    id = property(fget=get_id, fset=set_id)

    def get_currentCellId(self) -> int:
        return self._currentCellId
    
    def set_currentCellId(self, currentCellId: int):
        self._currentCellId = currentCellId

    currentCellId = property(fget=get_currentCellId, fset=set_currentCellId)
