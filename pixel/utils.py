import numpy as np
import autoit
import logging
import json

logger = logging.getLogger("PIXEL")


""" Définition des constantes """
## Pour la CAT
FIRST_FLAG = (242, 372) #Coordonnées du premier drapeau (!!! ne pas bouger la fenêtre)
DELTA_FLAG = (0,23) #Distance verticale entre 2 drapeaux
DELTA_CHECK = (-11,32) #Ecarts entre le bouton "valider", et le dernier drapeau

DIRECTION = {0 : (1319,398), 2 : (735,730), 4 : (269,410), 6 : (783,30)}
            #gauche     bas        droite     haut
DIRECTION2 = {0 : 'right', 2 : 'bottom', 4 : 'left', 6 : 'top'}


MIN_X, MAX_X, MIN_Y, MAX_Y = 270, 1265, 20, 725 #extreme coordinates of the map cells, obtained with AutoIt Window Info
CELL_HEIGHT = (MAX_Y-MIN_Y)/20.5 #34,39
CELL_WIDTH = (MAX_X-MIN_X)/14.5 #68,62

## Pour les déplacements sauvages


""" Fonctions génériques """

def loadGameWindow(gameTitle = 'Nobelle'):
    GAME = autoit.win_get_title(gameTitle) # Get The Full Title
    if(GAME):
        logger.info("Window [" + GAME + "] successfully linked to AutoIt")
        print("Window [" + GAME + "] successfully linked to AutoIt")
    else:
        logger.warning('Window not found for AutoIt')

    return str(GAME)

"""
    Apparently, on the y-axis, pixel 256 for AutoIt is pixel 279 for Dofus. I correct
"""


def sendClick(GAME, x, y, clicks=1): #Click to this coordinates in the loaded game window
    logger.info("Proceed to click " + str(clicks) + " time(s) in " + str(x) + ", " + str(y) + " in " + GAME)
    autoit.control_click(title=GAME, text="", control="", clicks=clicks, button='left', x=x, y=y-23)

def sendKeyboard(GAME, sequence): #Type this raw sequence in the loaded game window
    logger.info("Sends " + sequence + " to the keyboard")
    print("On a tapé " + sequence   + " à " + GAME)
    autoit.control_send(GAME, "", sequence)


"""
    Obtenir le texte à partir de l'id
    Idéalement, à faire custom
"""
with open('data/PointOfInterest.json') as poi_json:
    poi_dict = json.load(poi_json)

poi_json.close()

with open('data/i18n_fr.json', encoding='utf-8') as text_json:
    text_dict = json.load(text_json)

text_json.close()

def getText(id) -> str:
    for p in poi_dict:
        if p["id"] == id:
            return str(text_dict["texts"][str(p["nameId"])])
    logger.warning("No text found for the id " + str(id) + ". Maybe update the json document")
    raise Exception("Text not found !!!")

def findCellCenterCoordinates(cellNumber: int, direction: int = -1) -> tuple[int, int]:
    '''
        Parameters:
            cellNumber: (int) between 0 and 559
            direction: (int) 0,2,4,6, or something else, 
        Output: 
            Precise coordinates to click on in order to perform a movement
            If direction is 0, 2, 4, 6, it is a map change, else it's not
    '''
    rowY = cellNumber//14 #between 0 and 39
    rowX = cellNumber%14 #between 0 and 13

    coordY =  CELL_HEIGHT * (rowY//2) + (rowY%2 + 1)*CELL_HEIGHT/2 + MIN_Y #464,265
    coordX = CELL_WIDTH * rowX + (rowY%2 + 1)*CELL_WIDTH/2 + MIN_X #240,17

    if direction==0:
        coordX += CELL_WIDTH
    elif direction==4:
        coordX -= CELL_WIDTH
    elif direction == 2:
        if rowY==13:
            coordY += CELL_HEIGHT*0.2
        else:
            coordY += CELL_HEIGHT*0.8
    elif direction == 6:
        if rowY==0:
            coordY -= CELL_HEIGHT*0.2
        else:
            coordY -= CELL_HEIGHT*0.8

    return (int(coordX), int(coordY))


b: int = 0
startX: int = 0
startY: int = 0
cell: int = 0

MAP_HEIGHT = 20
MAP_WIDTH = 14
idToPos = {}
posToId = {}

for a in range(MAP_HEIGHT):
    for b in range(MAP_WIDTH):
        idToPos[cell] = (startX + b, startY + b)
        posToId[(startX + b, startY + b)] = cell
        cell += 1
    startX += 1
    for b in range(MAP_WIDTH):
        idToPos[cell] = (startX + b, startY + b)
        posToId[(startX + b, startY + b)] = cell
        cell += 1
    startY -= 1

def coordToCellId(x: int, y: int) -> int:
    return (x - y) * MAP_WIDTH + y + (x - y) / 2
      
def cellIdToCoord(cellId: int) -> tuple[int, int]:
    return idToPos[cellId]