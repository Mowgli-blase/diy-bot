from threading import Thread, Event
from queue import Queue
from time import sleep
import logging
from enum import Enum
import autoit

from pixel.actions import *
from pixel.utils import *
from tabs.cat import *
from tabs.Fight import Fighter
from client import PlayerCharacterManager

logger = logging.getLogger("PIXEL")


class PixelClicker(Thread):
    """
    get an action queue that may be fed by other threads. Currently, I think it is just going to feed itself
    TODO: I think I may have to review the way to manage actions (check if something is true with image recognition...)
    """

    class Flag(Enum):
        mapChanged = 1
        zaapLoaded = 2
        hintSniffed = 3
        inHS = 4
        notMoving = 5
        notInFight = 6

        turnStarted = 8
        fighterMode = 9
        groupFound = 10


    def __init__(self, action_queue: Queue, stop_event: Event=None, gameTitle: str="Nobelle") -> None:
        super().__init__()
        self.stop_event = stop_event
        self.actions = action_queue

        self.FLAGS: dict[PixelClicker.Flag, bool] = { flag : True for flag in PixelClicker.Flag}

        self.state = True
        self.hint = {'id' : 666, 'phorreur' : False, 'totalStepCount' : 5, 'checkPointCurrent' : 0, 'checkPointTotal' : 3, 'direction' : 6, 'startMapX' : 1, 'startMapY' : 4}
        self.current_X, self.current_Y = (0,0)
        self.phorreur_found = False
        
        self.groupCellId: int = None

        try:
            self.GAME = loadGameWindow(gameTitle)
            self.pcm = PlayerCharacterManager.PlayedCharacterManager(gameTitle)

        except Exception as e:
            logger.warning("Erreur au lancement de AutoIt : %s", e)

        if self.actions.empty():
            """
                Ici, initier une action par défaut ?
            """

    def setParserThread(self, parserThread : Thread):
        self.parserThread = parserThread

    def run(self):
        logger.info("Pixel started...")
        logger.info("The loaded game is " + self.GAME)
        try:
            while not self.stop_event or not self.stop_event.is_set():
                # Try to make the actions as small as possible so you are able to quit gently at anytime

                if self.actions.empty():
                    sleep(1)
                    continue

                action = self.actions.get()
                action.execute()
                next_action = action.next_action()
                if next_action is not None:
                    self.actions.put(next_action)

        except Exception as e:
            logger.fatal(e)
        finally:
            if self.stop_event is not None:
                self.stop_event.set()
            logger.info("Pixel killed.")
            print("\tPixel killed.")

    def waitForFlag(self, flag: Flag):
        self.FLAGS[flag] = False
        while not self.FLAGS[flag]:
            sleep(0.1)
        sleep(1)


    def unlockFlag(self, flag: Flag):
        '''
            Should be called outside of the class
        '''
        self.FLAGS[flag] = True

    def setGroupCellId(self, cellId: int):
        self.groupCellId = cellId

    def initiateFight(self):
        '''
            Should called just before entering a fight
        '''
        print("INITIATE FIGHT")
        self.fighter = Fighter(self.GAME)
        self.FLAGS[PixelClicker.Flag.notInFight] = False
        print("BEGIN FIGHT")

    def setPlayerCellId(self, playerCellId: int):
        self.fighter.setPlayerCellId(playerCellId)

    def addMonsterCellId(self, monsterId: int, monsterCellId: int):
        self.fighter.addMonsterCellId(monsterId, monsterCellId)

    def removeMonsterId(self, monsterId: int):
        self.fighter.removeMonsterId(monsterId)

    def fight(self):
        self.fighter.endTurn()
        while not self.FLAGS[PixelClicker.Flag.notInFight]:
            self.waitForFlag(PixelClicker.Flag.turnStarted)
            print("TURN START")
            sleep(0.5)
            try:
                self.fighter.attack()
                # End turn
            except Exception as e:
                print(e)
            self.fighter.endTurn()
        
        sleep(1) # Add rather a flag to wait for map loading
        sendKeyboard(self.GAME, "{ESC}")


    def fighterMode(self):
        # Wait for a fight
        self.FLAGS[PixelClicker.Flag.fighterMode] = True
        while self.FLAGS[PixelClicker.Flag.fighterMode]:
            self.waitForFlag(PixelClicker.Flag.groupFound)
            self.startGroupFight(self.groupCellId)

            sleep(5)
            if not self.FLAGS[PixelClicker.Flag.notInFight]:
                self.fight()

    def startGroupFight(self, cellId):
        x, y = findCellCenterCoordinates(cellId)
        sleep(1)
        sendClick(self.GAME, x, y)

class BeginCat(Action):

    def __init__(self, GAME):
        super().__init__()
        self.GAME = GAME

    def execute(self) -> None:

        logger.info("Path to pick a CAT")
        #Appuyer sur H
        sendKeyboard(self.GAME, "h")
        PixelClicker.waitForFlag(PixelClicker.Flag.inHS)
        sendClick(self.GAME, 453, 350)
        PixelClicker.waitForFlag(PixelClicker.Flag.zaapLoaded)
        sleep(1)
        sendKeyboard(self.GAME, "Champs de Cania")
        sleep(0.5)
        sendKeyboard(self.GAME, "{ENTER}")
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        sendClick(self.GAME, 1450, 430)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        sendClick(self.GAME, 1450, 430)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        sendClick(self.GAME, 757, 408)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        sendClick(self.GAME, 1132, 390)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)

        #Réaliser le dialogue, et prendre la bonne chasse
        autoit.mouse_move(822, 392)
        sleep(1)
        sendClick(self.GAME, 822, 392)
        autoit.mouse_move(950, 460)
        sleep(1)
        sendClick(self.GAME, 950, 460) #Chasse de niveau 140
        sleep(1)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        sendClick(self.GAME, 1236, 646)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        sendClick(self.GAME, 1127, 682)
        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
        #Lire le premier indice, et se rendre aux bonnes coordonnées

        input("Press enter when you are to the correct coordinates")

    def next_action(self) -> Action:
        return DoCat(self.GAME)

class DoCat(Action):

    def __init__(self, GAME):
        super().__init__()
        self.GAME = GAME
        PixelClicker.current_X = PixelClicker.hint['startMapX']
        PixelClicker.current_Y = PixelClicker.hint['startMapY']

    def execute(self) -> None:
        logger.info("Starting the CAT")

        for i in range(PixelClicker.hint['checkPointTotal']-1):
            for j in range(PixelClicker.hint['totalStepCount']):

                if PixelClicker.hint['phorreur'] == True:
                    logger.debug("We should move until we find the Phorreur " + str(PixelClicker.hint['id']))
                    print("On est actuellement en " + str(PixelClicker.current_X) + ";" + str(PixelClicker.current_Y) + ".On va cherche le Phorreur " + str(PixelClicker.hint["id"]))
                    #Move to the Phorreur with a 'while'
                    PixelClicker.phorreur_found = False
                    while PixelClicker.phorreur_found == False:
                        sendClick(self.GAME, DIRECTION[PixelClicker.hint['direction']][0], DIRECTION[PixelClicker.hint['direction']][1])
                        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
                        if PixelClicker.hint['direction'] == 0:
                            PixelClicker.current_X +=1
                        if PixelClicker.hint['direction'] == 4:
                            PixelClicker.current_X -=1
                        if PixelClicker.hint['direction'] == 2:
                            PixelClicker.current_Y +=1
                        if PixelClicker.hint['direction'] == 6:
                            PixelClicker.current_Y -=1
                else:
                    print("On cherche " + str(PixelClicker.hint['id']))
                    logger.debug("Regular hint, let's find a " + getText(PixelClicker.hint['id']))
                    dist = getDistance(PixelClicker.current_X, PixelClicker.current_Y, getText(PixelClicker.hint['id']), DIRECTION2[PixelClicker.hint['direction']])
                    print("On est actuellement en " + str(PixelClicker.current_X) + ";" + str(PixelClicker.current_Y) + ". On va se déplacer de " + str(dist) + " maps pour rejoindre " + str(PixelClicker.hint["id"]))
                    logger.info("Moving from " + str(dist) + " maps to find the id " + str(PixelClicker.hint["id"]))
                    for i in range(dist):
                        sendClick(self.GAME, DIRECTION[PixelClicker.hint['direction']][0], DIRECTION[PixelClicker.hint['direction']][1])
                        PixelClicker.waitForFlag(PixelClicker.Flag.mapChanged)
                        if PixelClicker.hint['direction'] == 0:
                            PixelClicker.current_X +=1
                        if PixelClicker.hint['direction'] == 4:
                            PixelClicker.current_X -=1
                        if PixelClicker.hint['direction'] == 2:
                            PixelClicker.current_Y +=1
                        if PixelClicker.hint['direction'] == 6:
                            PixelClicker.current_Y -=1

                #Click a new flag
                sendClick(self.GAME, FIRST_FLAG[0]+j*DELTA_FLAG[0], FIRST_FLAG[1]+j*DELTA_FLAG[1], 1)
                PixelClicker.waitForFlag(PixelClicker.Flag.hintSniffed)
                logger.debug("We completed a FLAG of the cat")

            #Check a step
            sendClick(self.GAME, FIRST_FLAG[0]+(PixelClicker.hint['totalStepCount']-1)*DELTA_FLAG[0]+DELTA_CHECK[0], FIRST_FLAG[1]+(PixelClicker.hint['totalStepCount']-1)*DELTA_FLAG[1]+DELTA_CHECK[1])
            PixelClicker.waitForFlag(PixelClicker.Flag.hintSniffed)
            logger.debug("We completed a STEP of the cat")


        input("Ready to begin the fight !")


    def next_action(self) -> Action:
        return

class Fight(Action):
    def __init__(self, GAME):
        super().__init__()
        self.GAME = GAME


    def execute(self) -> None:
        return

    def next_action(self) -> Action:
        return


class MoveToCell(Action):
    def __init__(self, GAME, cellId: int):
        super().__init__()
        self.GAME = GAME
        self.cellId = cellId

    def execute(self) -> None:
        coordinates = findCellCenterCoordinates(self.cellId)
        sendClick(self.GAME, coordinates[0], coordinates[1])
        return

    def next_action(self) -> Action:
        return

if __name__ == "__main__":
    action_queue = Queue()
    """
        Ici, initier une action par défaut ?
    """

    # action_queue.put(ClickOnUnclickedCategories())
    pixel = PixelClicker(action_queue)
    sleep(1)
    pixel.start()