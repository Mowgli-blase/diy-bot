import logging
from queue import Queue

from pixel.utils import *
from tabs.cat import *

logger = logging.getLogger("PIXEL")


class Action():
    '''
        Interface for atomic actions
    '''
    def execute(self) -> None:
        raise NotImplementedError("You need to override this")
    def next_action(self):
        """
            output : Next action or None
        """
        raise NotImplementedError("You need to override this")


    ### Actions atomiques

class MoveToNextMap(Action):
    def __init__(self, GAME, moves : Queue):
        super().__init__()
        self.GAME = GAME
        try:
            self.moves = moves
        except:
            print("Meeerde")

    def execute(self) -> None:
        self.moves.get()
        print("\t\tCLICK !")
        # APP.log("Click !", 3)

        #Cliquer où il faut
        #Check for map change

        return

    def next_action(self) -> Action:
        if not(self.moves.empty()):
            return MoveToNextMap(self.moves)
        else:
            return