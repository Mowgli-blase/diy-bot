import json
from random import random

with open('./data/135427.json') as f:
    cellsDict = json.load(f)
    cells = cellsDict['cells']


import matplotlib.pyplot as plt

XGRAD = 20
YGRAD = 10

SIDE = (XGRAD**2 + YGRAD**2)**0.5

WIDTH = 200
HEIGHT = 300

zoneColors = {}
def drawCell(i):
    # print("x y", x, y)
    # print("goto", x*XGRAD*2-WIDTH, y*YGRAD*2 - HEIGHT)
    
    cell = cells[i]
    
    if i%28 < 14: x_offset = 0 #ligne paire, décallée à droite
    else: x_offset = 0.5

    x = i%14 + x_offset
    y = 40-i//14

    color = '#dbdbdb'
    if cell['mapChangeData']: color = '#00d0ff'
    if cell['moveZone']: color = 'pink'
    if cell['farmCell']: color = 'green'
    if not cell['mov']: color = 'black'
    if '_linkedZone' in cell:
        if cell['_linkedZone'] in zoneColors:
            color = zoneColors[cell['_linkedZone']]
        else:
            color = (random(),random(),random())
            zoneColors[cell['_linkedZone']] = color

    plt.fill([x, x+1/2, x+1,x+1/2], [y, y+1, y, y-1], color=color)

def drawMap():
    for i in range(len(cells)):
        drawCell(i)

    plt.axis('off')
    plt.show()

drawMap()
print(cells[196])
print(list(set(cell['_linkedZone'] for cell in cells if '_linkedZone' in cell)))