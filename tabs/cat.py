"""

    Manages the Requests with Dofus Map

"""


import requests, ast
import tkinter as tk
import time
import logging

from pixel.utils import *

logger = logging.getLogger("TABS - Cat")

with open("data/text.txt", "r") as file:
    contents = file.read()
    text = ast.literal_eval(contents)

def getDistance(current_X, current_Y, text_hint, direction, world=0):
    """
        Comment lire ces 3 valeurs 4 (5) valeurs
    """
    r = requests.get('https://dofus-map.com/huntTool/getData.php?x=' + str(current_X )+ '&y=' + str(current_Y) + '&direction=' + direction + '&world=' + str(world) + '&language=fr')

    hints = r.json()["hints"]
    min_dist = len(text_hint)
    min_arg = -1

    for i in range(len(hints)):
        dist = levenshtein(text_hint, text[str(hints[i]['n'])])
        if dist < min_dist:
            min_dist = dist
            min_arg = i
    return hints[min_arg]['d']


def levenshtein(mot1,mot2):
    ligne_i = [ k for k in range(len(mot1)+1) ]
    for i in range(1, len(mot2) + 1):
        ligne_prec = ligne_i
        ligne_i = [i]*(len(mot1)+1)

        for k in range(1,len(ligne_i)):
            cout = int(mot1[k-1] != mot2[i-1])
            ligne_i[k] = min(ligne_i[k-1] + 1, ligne_prec[k] + 1, ligne_prec[k-1] + cout)

    return ligne_i[len(mot1)]

def getPoi(index):
    return text[str(index)]