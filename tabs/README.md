## Fight

Ce module devra recevoir les infos sur la map, les sorts, la position des monstres et du perso, et finir le combat.

Dans un premier temps, l'objectif sera de gérer bêtement des combats simples, on verra plus tard pour lui faire prendre des décisions compliquées.

En gros : <br>
- Attaquer si possible
- Sinon marcher
- Ré-essayer d'attaquer

GameRolePlayAttackMonsterRequestMessage : 
{
    '__type__': 'GameRolePlayAttackMonsterRequestMessage', 'monsterGroupId': -20005.0
}

GameFightShowFighterMessage : 
{
    '__type__': 'GameFightShowFighterMessage',
 'informations': {'__type__': 'GameFightCharacterInformations',
                  'alignmentInfos': {'__type__': 'ActorAlignmentInformations',
                                     'alignmentGrade': 0,
                                     'alignmentSide': 0,
                                     'alignmentValue': 0,
                                     'characterPower': 460204540121.0},
                  'breed': 9,
                  'contextualId': 460204540111.0,
                  'disposition': {'__type__': 'FightEntityDispositionInformations',
                                  'carryingCharacterId': 0.0,
                                  'cellId': 415,
                                  'direction': 1},
                  'hiddenInPrefight': False,
                  'ladderPosition': 0,
                  'leagueId': 65535,
                  'level': 10,
                  'look': {'__type__': 'EntityLook',
                           'bonesId': 1,
                           'indexedColors': [28280399,
                                             35744840,
                                             57653397,
                                             68237350,
                                             89360751],
                           'scales': [140],
                           'skins': [90, 2140, 460, 461, 462, 1250],
                           'subentities': []},
                  'name': 'Antipods',
                  'previousPositions': [],
                  'sex': False,
                  'spawnInfo': {'__type__': 'GameContextBasicSpawnInformation',
                                'alive': True,
                                'informations': {'__type__': 'GameContextActorPositionInformations',
                                                 'contextualId': 460204540111.0,
                                                 'disposition': {'__type__': 'FightEntityDispositionInformations',
                                                                 'carryingCharacterId': 0.0,
                                                                 'cellId': 415,
                                                                 'direction': 1}},
                                'teamId': 0},
                  'stats': { ... },
                  'status': {'__type__': 'PlayerStatus', 'statusId': 10},
                  'wave': 0}
}

GameFightReadyMessage : 
{'__type__': 'GameFightReadyMessage', 'isReady': True}

GameEntitiesDispositionMessage :
{
    '__type__': 'GameEntitiesDispositionMessage',
 'dispositions': [{'__type__': 'IdentifiedEntityDispositionInformations',
                   'cellId': 483,
                   'direction': 5,
                   'id': -1.0},
                  {'__type__': 'IdentifiedEntityDispositionInformations',
                   'cellId': 467,
                   'direction': 5,
                   'id': -2.0},
                  {'__type__': 'IdentifiedEntityDispositionInformations',
                   'cellId': 173,
                   'direction': 1,
                   'id': 460204540111.0}]
}

GameFightTurnReadyMessage : 
{'__type__': 'GameFightTurnReadyMessage', 'isReady': True}





