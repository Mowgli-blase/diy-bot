from pixel.utils import *
from time import sleep


class Fighter:

    '''
    
    '''


    def __init__(self, GAME):

        self.playerCellId: int = -1
        self.monstersCellsIds: dict[int, int]= {}

        self.GAME = GAME

        
        ###
        # HARDCODED INFORMATIONS
        # This should be definitely improved
        ###

        self.skillId: str = "'"
        self.skillScope: int = 5
        self.skillUses: int = 1
    
    def setPlayerCellId(self, playerCellId: int):
        self.playerCellId = playerCellId

    def addMonsterCellId(self, monsterId: int, monsterCellId: int):
        self.monstersCellsIds[monsterId] = monsterCellId

    def removeMonsterId(self, monsterId: int):
        self.monstersCellsIds.pop(monsterId)

    def endTurn(self):
        sendKeyboard(self.GAME, "{F1}")

    def attackMonster(self, monsterId: int):
        monsterCellId = self.monstersCellsIds[monsterId]
        sendKeyboard(self.GAME, self.skillId)
        sleep(0.5)
        x,y = findCellCenterCoordinates(monsterCellId)
        sendClick(self.GAME, x, y)
        print("Attack monster " + str(monsterId) + " at cell " + str(monsterCellId) + " (" + str(x) + ", " + str(y) + ")")

    def attack(self):
        pX, pY = cellIdToCoord(self.playerCellId)
        closestMonsterId = min(self.monstersCellsIds, key=lambda monsterId: abs(cellIdToCoord(self.monstersCellsIds[monsterId])[0] - pX) + abs(cellIdToCoord(self.monstersCellsIds[monsterId])[1] - pY))

        # mX, mY = cellIdToCoord(closestMonsterCellId)
        # if abs(pX - mX) + abs(pY - mY) <= self.skillScope:
        for _ in range(self.skillUses):
            if closestMonsterId in self.monstersCellsIds:
                self.attackMonster(closestMonsterId)
                sleep(1)