from tkinter import *
import tkinter as tk
from tkinter import ttk # gérer les onglets
import tkinter.scrolledtext as st
from tkinter import messagebox

import logging


logger = logging.getLogger("APP")
_version = '0.6'


class App(Tk):

    def __init__(self):
        Tk.__init__(self)
        self.config(bg='#b6eafa')
        self.geometry("800x800+500+100")
        self.resizable(width=False, height=False)
        self.title("Origami")
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        ## Définition des variables

        self.accountName = StringVar()
        self.password = StringVar()



        self.buildOnglets()
        self.buildConsole()
        self.buildCatFrame()
        logger.info("App initialized ...")




    ## Définition des fonctions

    def log(self, msg, tab=0):
        self.console.configure(state='normal')
        self.console.insert(tk.INSERT, "> " + " "*tab + msg + "\n")
        self.console.see(tk.END)
        self.console.configure(state='disabled')


    def logIn(self):
        if self.accountName.get():
            self.infoLabel['text'] = "Compte connecté : " + self.accountName.get()
            self.log("> Connexion au compte " + self.accountName.get())

    def selectCatLevel(self, event):
        msg = f'Chasse de niveau {self.levelCombobox.get()} !'
        self.log(msg)

    def startCat(self):
        self.log("Début de la chasse")


    def buildOnglets(self):
        ## Création des onglets
        self.style = ttk.Style(self)
        self.style.configure('TNotebook', tabposition='nw')

        self.notebook = ttk.Notebook(self, width=800, height=400, style='TNotebook')   # Création du système d'onglets
        self.catFrame = tk.Frame(self, bg="red")
        self.f2 = tk.Frame(self, bg="blue")
        self.notebook.add(self.catFrame, text="[ Chasse au trésor ]")
        self.notebook.add(self.f2, text="Coming soon ...")

        self.notebook.grid(row=0, column=0, sticky='nw')

    def buildCatFrame(self):

                ## Configuration catFrame

        self.accountNameLabel = Label(self.catFrame, text="Nom de Compte :")
        self.accountNameLabel.grid(row=0, column=0, pady='15', sticky='nesw')

        self.accountNameText = Entry(self.catFrame, textvariable=self.accountName)
        self.accountNameText.grid(row=0, column=1, sticky='w')

        self.passwordLabel = Label(self.catFrame, text="Mot de passe :")
        self.passwordLabel.grid(row=1, column=0, sticky='nsew')

        self.passwordText = Entry(self.catFrame, textvariable=self.password)
        self.passwordText = Entry(self.catFrame, show="*")
        self.passwordText.grid(row=1, column=1, sticky='w')

        self.logInButton = Button(self.catFrame, text="Log In", command=self.logIn)
        self.logInButton.grid(row=1, column=3, padx=(5,0), sticky='ew')


        self.infoLabel = Label(self.catFrame, width=40, height=4, text="Connexion en attente")
        self.infoLabel.grid(row=0, column=4, rowspan=2, padx=(10,0), sticky='ew')


        self.levelLabel = Label(self.catFrame, text="Niveau de la chasse")
        self.levelLabel.grid(row=2, column=0, pady=(20,0), sticky='sew')

        self.catFrame.level = StringVar()
        self.levelCombobox = ttk.Combobox(self.catFrame, textvariable=self.catFrame.level)
        self.levelCombobox['values'] = ('20', '40', '60','80', '100', '120', '140', '160', '180', '200')
        self.levelCombobox['state'] = 'readonly'
        self.levelCombobox.bind('<<ComboboxSelected>>', self.selectCatLevel)
        self.levelCombobox.set('20')
        self.levelCombobox.grid(row=3, column=0, sticky='n')

        self.startButton = Button(self.catFrame, text="Démarrer", command=self.startCat)
        self.startButton.grid(row=3, column=3, padx=(5,0), sticky='ew')


    def buildConsole(self):
        ## Configuration console
        self.console = st.ScrolledText(self, wrap = 'word', width = 80, height = 20, font = ("Helvetica",10), bg='#000000', fg='#25e022', relief='sunken')
        self.console.insert(tk.INSERT,">_ Bot V"+_version+" loaded \n>_\n")
        self.console.configure(state='disabled')
        self.console.grid(row=5, column=0)
        self.console.visible = True

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.destroy()