import logging, json

logger = logging.getLogger("MessageParser")

"""
    Permet d'obtenir des coordonnées à partir d'une mapId
    Il faut encore gérer le paramètre worldMap
"""

with open('data/MapPositions.json') as mapPositions_json:
    mapPositions_dict = json.load(mapPositions_json)

mapPositions_json.close()

def getMapCoordiates(mapId, worldMap = 1): #Il faut gérer aussi le world !!
    for m in mapPositions_dict:
        if m['id']==mapId:
            return (m['posX'], m['posY'])

    logging.warning("Map id couldn't be found")



