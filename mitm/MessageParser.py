from threading import Thread, Event
from queue import Queue
import logging
from scapy.packet import Raw

from labot.sniffer.network import Msg, sniff, on_receive
from pixel.PixelClicker import PixelClicker
from tabs.cat import *
from tabs.Farmer import Farmer
from mitm.utils import *

from enum import Enum

logger = logging.getLogger("MessageParser")

class IncompleteData(Exception):
    pass

class MessageParser(Thread):
    def __init__(self, message_queue: Queue, stop_event: Event = None) -> None:
        super().__init__()
        self.queue = message_queue
        self.stop_event = stop_event

        self.state = True # What is this ?
        self.incomingPlayerCellId = None

    def setPixelThread(self, pixelThread : Thread):
        self.pixelThread = pixelThread

    def extract_hint(self, message: dict):
        """
        Parse the message to extract the hint
        """

        knownStepsList = message.get("knownStepsList")
        ret = {}
        ret['totalStepCount'] = message.get("totalStepCount")
        ret['checkPointCurrent'] = message.get("checkPointCurrent")
        ret['checkPointTotal'] = message.get("checkPointTotal")

        current_step = knownStepsList[len(knownStepsList)-1]
        if current_step['__type__'] == 'TreasureHuntStepFight':
            logger.info("We reached the end of the CAT")
            print("Combat final atteint")
            return

        ret['direction'] = current_step['direction']
        pos = getMapCoordiates(message.get("startMapId"))
        ret['startMapX'] = pos[0]
        ret['startMapY'] = pos[1]
        try:
            ret['id'] = current_step['poiLabelId']
            ret['phorreur'] = False
            return ret
        except:
            ret['id'] = current_step['npcId']
            ret['phorreur'] = True
            return ret


    def try_phorreur_found(self, id, message: dict):
        """
        Parse the message to check if a Phorreur is present
        """
        
        try:
            actors = message.get("actors")
            for a in actors:
                if 'npcId' in a:
                    self.pixelThread.phorreur_found = True
                    return True
            self.pixelThread.phorreur_found = False
            return False
        except:
            logger.warning("Error during the search for a Phorreur !")


    def extract_data(self, message: dict) -> list[dict]:
        """
        Extracts the item data from the HDV message
        """

        item_prices = []
        item_lines = message.get("itemTypeDescriptions")
        # item_lines represents the lines in the HDV
        for line in item_lines:
            objectGID = line.get("objectGID")
            objectType = line.get("objectType")
            prices = line.get("prices")

            if objectGID is None or objectType is None or prices is None:
                raise IncompleteData("")

            item = {
                "category": objectType,
                "item_gid": objectGID
            }

            for index, price in enumerate(prices):
                if not price:
                    continue
                item_prices.append({**item, "price": price, "quantity": 10**index})

        return item_prices



    def run(self):
        logger.info("Message parser started...")

        def on_msg(message: Msg) -> None:
            message = message.json()
            logger.debug(f"Sniffed message: {message}")
            message_type = message.get("__type__")

            if message_type == "MapComplementaryInformationsDataMessage":
                logger.info("Entered a new map")

                """ Gestion de la cat """
                if(self.pixelThread.hint['phorreur'] == True):
                    if self.try_phorreur_found(self.pixelThread.hint['id'], message):
                        logger.info("Phorreur found on this map !")

                """ Gestion du farming """
                for ie in message.get("interactiveElements"):
                    for se in message.get("statedElements"):
                        if se["elementId"] == ie["elementId"]:

                            res = {}
                            res['interactive'] = ie
                            res['stated'] = se
                            Farmer._resLst.append(res)

                """ Gestion du playerCharacterManager """
                for actor in message.get("actors"):
                    if actor.get("name") == self.pixelThread.pcm.playerName:
                        self.pixelThread.pcm.id = actor.get("contextualId")
                        self.pixelThread.pcm.cellId = actor.get("disposition").get("cellId")

                self.pixelThread.unlockFlag(PixelClicker.Flag.mapChanged)

            elif message_type == "InteractiveElementUpdatedMessage":
                print("InteractiveElementUpdatedMessage trouvé")

            elif message_type == "MapComplementaryInformationsDataInHavenBagMessage":
                logger.info("Entered the HS")
                self.pixelThread.unlockFlag(PixelClicker.Flag.inHS)

            elif message_type == "ZaapDestinationsMessage":
                logger.info("Displayed the Zaap informations")
                self.pixelThread.unlockFlag(PixelClicker.Flag.zaapLoaded)

            elif message_type == "TreasureHuntMessage":
                temp_hint = self.extract_hint(message)
                self.pixelThread.hint = temp_hint if temp_hint != None else self.pixelThread.hint
                self.pixelThread.unlockFlag(PixelClicker.Flag.hintSniffed)

            elif message_type == "GameFightStartingMessage":
                self.pixelThread.pcm.id = message.get("attackerId")
                
                # self.pixelThread.startFight()

            elif message_type == "GameEntitiesDispositionMessage":
                """
                Several such message are sent when entering a fight. They must be merged to get all informations
                """
                for disposition in message.get("dispositions"):
                    cellId, id = disposition.get("cellId"), disposition.get("id")
                    if id == self.pixelThread.pcm.id: # If it's the player
                        self.pixelThread.setPlayerCellId(cellId)
                    else:
                        self.pixelThread.addMonsterCellId(id, cellId)
            
            elif message_type == "GameActionFightSlideMessage":
                
                pass

            elif message_type == "GameActionFightDeathMessage":
                if message.get("targetId") == self.pixelThread.pcm.id:
                    # self.pixelThread.unlockFlag(PixelClicker.Flag.playerDead)
                    pass
                else:
                    self.pixelThread.removeMonsterId(message.get("targetId"))

            elif message_type == "GameFightJoinMessage":
                self.pixelThread.initiateFight()

            elif message_type == "GameFightTurnStartMessage":
                if message.get("id") == self.pixelThread.pcm.id:
                    self.pixelThread.unlockFlag(PixelClicker.Flag.turnStarted)

            elif message_type == "GameFightEndMessage":
                self.pixelThread.unlockFlag(PixelClicker.Flag.notInFight)
                self.pixelThread.unlockFlag(PixelClicker.Flag.turnReady) # Sometimes, causes a bug otherwise


            elif message_type == "GameMapMovementMessage":
                """
                Used to track the player cell id
                """
                id = message.get("actorId")
                if self.pixelThread.FLAGS[PixelClicker.Flag.notInFight]:
                    if id == self.pixelThread.pcm.id:
                        self.incomingPlayerCellId = message.get("keyMovements")[-1]
                    elif self.pixelThread.FLAGS[PixelClicker.Flag.fighterMode]:
                        self.pixelThread.setGroupCellId(message.get("keyMovements")[-1])
                        self.pixelThread.unlockFlag(PixelClicker.Flag.groupFound)
                else:
                    if id == self.pixelThread.pcm.id:
                        self.pixelThread.setPlayerCellId(message.get("keyMovements")[-1])
                    else:
                        self.pixelThread.addMonsterCellId(id, message.get("keyMovements")[-1])
                
            elif message_type == "GameMapMovementConfirmMessage":
                """
                Triggered when the player cell id is confirmed
                """
                self.pixelThread.unlockFlag(PixelClicker.Flag.notMoving)
                self.pixelThread.pcm.cellId = self.incomingPlayerCellId
                # print("Player cell", self.pixelThread.pcm.cellId)
                
            else:
                logger.debug(f"Unknown message type: {message_type}")
                # print(f"Unknown message type: {message_type}")

        sniff(
            filter="tcp port 5555",
            lfilter=lambda p: p.haslayer(Raw),
            stop_event=self.stop_event,
            prn=lambda p: on_receive(p, on_msg),
        )

        logger.info("Message parser killed.")
        print("\tMessage parser killed.")