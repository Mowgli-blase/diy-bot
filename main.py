from queue import Queue
import threading
from time import sleep, strftime

import logging
from mitm.MessageParser import MessageParser
from pathfinding.main import WorldPathFinder, Mock
from pixel.PixelClicker import PixelClicker, BeginCat, DoCat, MoveToCell
from pixel.actions import *

from data.DataExtractor import DataExtractor

from app.App import App

logger = logging.getLogger("MAIN") #Aucune idée de comment gérer ça mdr
logging.basicConfig(format='%(levelname)s ~ [%(name)s] : %(message)s', filename='execution.log', encoding='utf-8', level=logging.INFO)

logging.info('Program successfully started at %s !', strftime('%d/%m/%y %H:%M:%S'))

if __name__ == "__main__": #cette ligne indique que le code ne sera exécuté que si on lance le programme depuis ici

    ###############
    gameTitle = "Jean-Francisse"  # Name if the game character
    ###############

    message_queue = Queue()
    action_queue = Queue()

    stop_event = threading.Event()

    mitm = MessageParser(message_queue=message_queue, stop_event=stop_event)
    pixel = PixelClicker(action_queue=action_queue, stop_event=stop_event, gameTitle=gameTitle)
    mitm.setPixelThread(pixel)
    pixel.setParserThread(mitm)

    logger.info("Bot is going to start...")
    print("Bot is going to start...")
    mitm.start()
    pixel.start()

    # APP.mainloPop()

    # pixel.actions.put(BeginCat(PixelClicker.GAME))
    # pixel.actions.put(DoCat(PixelClicker.GAME))
    # pixel.actions.put(Test(PixelClicker.GAME))

    WorldPathFinder.init(DataExtractor())

    print(" [ OUIII ] Path finished :) !")

    try:
        pixel.fighterMode()
        
        while not stop_event.is_set():                
            sleep(1)
    except KeyboardInterrupt:
        logger.info("Keyboard interrupt, gracefully stopping")
        stop_event.set()
        mitm.join()
        pixel.join()

    logger.info("Bot stopped.")
    print("Bot stopped.")


    
    
    # WorldPathFinder.init(DataExtractor())

    # startMapId = 154010372.0 # Incarnam [-1, -4]
    # startCellId = 75 # Arbitrary choice
    
    # endMapId = 154010884.0 # Incarnam [-2, -4]

    # for step in Mock.getPath(startMapId, startCellId, endMapId): #3 -19 outdoor to 5 -19 outdoor
    #     print(step)

    # dataExtractor = DataExtractor()
    # data = dataExtractor.load_dlm_map_by_id(startMapId)
    # cells = data.get("cells")

