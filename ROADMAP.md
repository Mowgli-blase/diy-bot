Roadmap

- [ ] MessageParsing

    Allow the bot to parse the messages from the game.

    - [ ] Parse character informations
        With {'__type__': 'CharacterSelectionMessage', 'id': 176823599397}

    - [ ] Parse current map informations

- [x] Pathfinding

    Allow the bot to find the shortest path between two maps, and the list of actions to perform to go from one map to another.

- [x] Clicker

    Allow the bot to click on the window to perform ingame actions.

- [ ] Fighter

    Allow the bot to manage the fights.
    - [ ] Begin fight
      - [ ] Locate monsters on map
      - [ ] Successfully click on monster
    - [ ] Locate player in fight
    - [ ] Locate monsters in fight
    - [ ] Perform moves
    - [ ] Perform attacks

- [ ] Mob farm

    Allow the bot to farm mobs repetitively.

- [ ] Treasure hunt

    Allow the bot to find treasures.