import os
from data.pydofus.dlm import DLM, InvalidDLMFile

import matplotlib.pyplot as plt
from random import random

class DataExtractor:

    """
    Read DLM files and extract map data from them
    
    Requires to extract all dlm from dofus client
    """
    XGRAD = 20
    YGRAD = 10

    SIDE = (XGRAD**2 + YGRAD**2)**0.5

    WIDTH = 200
    HEIGHT = 300

    zoneColors = {}


    def __init__(self):
        self.dlm_path_dict = DataExtractor.get_dlm_path_dict()

    @classmethod
    def get_dlm_path_dict(cls) -> dict:
        mainDir = "C:/Users/Baptiste/Documents/Cash Machine/PyDofus/output/"
        dlm_path_dict = {}

        for mapDir in os.listdir(mainDir):
            if '.d2p' in mapDir:
                for subDir in os.listdir(mainDir + '/' + mapDir):
                    for mapFile in os.listdir(mainDir + '/' + mapDir + '/' + subDir):
                        dlm_path_dict[int(mapFile.split('.')[0])] = mainDir + mapDir + '/' + subDir + '/' + mapFile

        return dlm_path_dict

    def load_dlm_map_by_id(self, id: int) -> dict:
        id = int(id)
        with open(self.dlm_path_dict[id], "rb") as dlm_input:
            dlm = DLM(dlm_input, "649ae451ca33ec53bbcbcc33becf15f4")
            data = dlm.read()
        return data
    
    def drawCell(self, i, cells):
        # print("x y", x, y)
        # print("goto", x*XGRAD*2-WIDTH, y*YGRAD*2 - HEIGHT)
        
        cell = cells[i]
        
        if i%28 < 14: x_offset = 0 #ligne paire, décallée à droite
        else: x_offset = 0.5

        x = i%14 + x_offset
        y = 40-i//14

        color = '#dbdbdb'
        if cell['mapChangeData']: color = '#00d0ff'
        if cell['moveZone']: color = 'pink'
        if cell['farmCell']: color = 'green'
        if not cell['mov']: color = 'black'
        if '_linkedZone' in cell:
            if cell['_linkedZone'] in self.zoneColors:
                color = self.zoneColors[cell['_linkedZone']]
            else:
                color = (random(),random(),random())
                self.zoneColors[cell['_linkedZone']] = color

        plt.fill([x, x+1/2, x+1,x+1/2], [y, y+1, y, y-1], color=color)

    def drawMap(self, cells):
        for i in range(len(cells)):
            self.drawCell(i, cells)

        plt.axis('off')
        plt.show()

    def visualizeMap(self, id):
        data = self.load_dlm_map_by_id(id)
        cells = data['cells']
        # self.drawMap(cells)
        print(data.keys())

if __name__ == "__main__":
    dataExtractor = DataExtractor()

    mapId = 154010372.0 # Incarnam [-1, -4]
    # mapId = 154010884.0 # Incarnam [-2, -4]


    dataExtractor.visualizeMap(mapId)