package com.ankamagames.dofus.network
{
   public class Metadata
   {
      
      public static const PROTOCOL_BUILD:String = "1.0.3-3286c75";
      
      public static const PROTOCOL_DATE:String = "Tue, 15 Jun 2021 13:52:28 +0000";
       
      
      public function Metadata()
      {
         super();
      }
   }
}
